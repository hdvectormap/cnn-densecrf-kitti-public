This software pertains to the research described in the CVPR 2016 paper:
Instance-Level Segmentation for Autonomous Driving with Deep Densely Connected MRFs
Ziyu Zhang, Sanja Fidler and Raquel Urtasun

This software is built on top of the DenseCRF project by Krähenbühl et al.

To compile the code, cd to densecrf, run:
 mkdir build
 cd build
 cmake -D CMAKE_BUILD_TYPE=Release ..
 make
 cd ..

For an example of running the code, cd to densecrf directory and execute RunInference.bash.

Make sure the paths are correct in RunInference.bash.
